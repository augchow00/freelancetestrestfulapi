﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TestAPI.Model;

namespace TestAPI.Repository
{
	public interface IPostRepository
	{
		Task<IEnumerable<Comment>> SearchComment(Predicate<Comment> predicate);
		Task<IEnumerable<Post>> GetPosts();
		Task<Post> GetPostById(long id);
		Task<IEnumerable<Comment>> GetComments();
		Task<Comment> GetCommentById(long id);		
	}
	public class PostRepository : IPostRepository
	{
		private const string URL = "https://jsonplaceholder.typicode.com/";
		HttpClient Client;
		public PostRepository()
		{
			Client = new HttpClient();
			Client.BaseAddress = new Uri(URL);
			Client.DefaultRequestHeaders.Accept.Add(
			new MediaTypeWithQualityHeaderValue("application/json"));
		}

		public async Task<IEnumerable<Comment>> SearchComment(Predicate<Comment> predicate)
		{
			var comments = await GetComments();
			var res = comments.ToList().FindAll(predicate);
			return res;
		}

		public async Task<IEnumerable<Post>> GetPosts()
		{
			var posts = await GetDataList(new List<Post>(), "posts");
			var comments = await GetComments();
			foreach (var post in posts)
			{
				var getComments = comments.Any() ? comments.Where(x => x.PostId == post.Id) : null;
				post.comments = getComments;
				post.TotalComment = getComments.Count();
			}
			return posts.OrderByDescending(x => x.TotalComment);
		}

		public async Task<Post> GetPostById(long id)
		{
			var res = await GetDataById(new Post(), "posts", id);
			return res;
		}

		public async Task<IEnumerable<Comment>> GetComments()
		{
			var res = await GetDataList(new List<Comment>(), "comments");
			return res;
		}

		public async Task<Comment> GetCommentById(long id)
		{
			var res = await GetDataById(new Comment(), "comments", id);
			return res;
		}

		private async Task<T> GetDataList<T>(T obj, string endpoint)
		{
			try
			{
				var response = await Client.GetAsync(endpoint);
				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					var deserializeContent = JsonConvert.DeserializeObject<T>(content);

					return deserializeContent;
				}
				else
				{
					throw new Exception("Failed to process data");
				}
			}
			catch (Exception ex)
			{

				throw new Exception($"Failed to get data, {ex.Message}");
			}
		}

		private async Task<T> GetDataById<T>(T obj, string endpoint, long id)
		{
			try
			{
				var response = await Client.GetAsync($"{endpoint}/" + id);
				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					var deserializeContent = JsonConvert.DeserializeObject<T>(content);

					return deserializeContent;
				}
				else
				{
					throw new Exception("Failed to process the post");
				}
			}
			catch (Exception ex)
			{

				throw new Exception($"Failed to get post, {ex.Message}");
			}
		}
	}
}
