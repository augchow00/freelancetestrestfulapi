﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestAPI.Model;
using TestAPI.Repository;

namespace TestAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class PostController : Controller
	{
		
		private readonly IPostRepository postRepository;
		public PostController(IPostRepository postRepository)
		{
			this.postRepository = postRepository;
		}

		[HttpGet("posts")]
		public async Task<IEnumerable<Post>> GetPosts()
		{
			var res = await postRepository.GetPosts();
			return res;
		}

		[HttpPost("search")]
		public async Task<IEnumerable<Comment>> SearchComments([FromBody] Comment param)
		{
			Predicate<Comment> checkId = c => c.Id == param.Id;
			Predicate<Comment> checkPostId = c => c.PostId == param.PostId;
			Predicate<Comment> checkEmail = c => c.Email == param.Email;
			Predicate<Comment> checkName = c => c.Name.Contains(param.Name);
			Predicate<Comment> checkBody = c => c.Body.Contains(param.Body);

			Predicate<Comment> CombineCheck = c => (checkId(c) || checkPostId(c) || checkEmail(c) ||
			param.Body != "" ? checkBody(c) : false || param.Name != "" ? checkName(c) : false);
			var res = await postRepository.SearchComment(CombineCheck);
			return res;
		}
	}
}
