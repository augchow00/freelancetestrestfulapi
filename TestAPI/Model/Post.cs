﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAPI.Model
{
	public class Post
	{
		public long UserId { get; set; }
		public long Id { get; set; }
		public string Title { get; set; }
		public string Body { get; set; }
		public int TotalComment { get; set; }
		public IEnumerable<Comment> comments { get; set; }
	}
	public class Comment
	{
		public long PostId { get; set; }
		public long Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Body { get; set; }		
	}

}
